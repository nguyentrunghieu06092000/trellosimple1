export interface Task {
  id: string;
  title: string;
}

export interface Columns {
  id: string;
  name: string;
  tasks: Task[];
}
export interface Board {
  id: string;
  name: string;
  columns: Columns[];
}
