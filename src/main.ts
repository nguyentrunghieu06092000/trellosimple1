import "bootstrap/dist/css/bootstrap.css";
import "bootstrap";
import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "primeicons/primeicons.css";
import PrimeVue from "primevue/config";
import "primevue/resources/themes/lara-light-indigo/theme.css";
import "primevue/resources/primevue.min.css";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.mount("#app");
app.use(PrimeVue);
