import { ref } from "vue";
import { defineStore } from "pinia";
import type { Board, Task, Columns } from "@/types";
import axios from "axios";

export const useTaskStore = defineStore("task", () => {
  const boards = ref<Array<Board>>([]);

  async function fetchTasks() {
    const data = await axios.get("/data/task.json");
    if (data.data) {
      boards.value = data.data;
    }
  }

  function addBoard(board: Board): void {
    boards.value.push(board);
  }

  function deleteBoard(boardID: string): void {
    boards.value = boards.value.filter((board) => board.id !== boardID);
  }

  function addTask(boardID: string, columnId: string, task: Task): void {
    const board = boards.value.find((board) => board.id === boardID);
    if (board) {
      const column = board.columns.find((col) => col.id === columnId);
      if (column) {
        column.tasks.push(task);
      }
    }
  }

  function deleteTask(boardID: string, columnId: string, taskId: string): void {
    const board = boards.value.find((board) => board.id === boardID);
    if (board) {
      const column = board.columns.find((col) => col.id === columnId);
      if (column) {
        column.tasks = column.tasks.filter((task) => task.id !== taskId);
      }
    }
  }

  function updateTask(
    boardID: string,
    columnId: string,
    taskID: string,
    newTitle: string
  ): void {
    const board = boards.value.find((col) => col.id === boardID);
    const column = board?.columns.find((col) => col.id === columnId);
    if (column) {
      const task = column.tasks.find((task) => task.id === taskID);
      if (task) {
        task.title = newTitle;
      }
    }
  }

  function deleteColumns(boardID: string, columnId: string): void {
    const board = boards.value.find((board) => board.id === boardID);
    if (board) {
      board.columns = board.columns.filter((col) => col.id !== columnId);
      console.log(board.columns);
    }
  }

  function addColumns(boardID: string, column: Columns) {
    const board = boards.value.find((board) => board.id === boardID);
    if (board) {
      board.columns.push(column);
    }
  }

  // ... other actions

  return {
    boards,
    fetchTasks,
    addTask,
    deleteTask,
    deleteColumns,
    addColumns,
    updateTask,
    addBoard,
    deleteBoard,
  };
});
