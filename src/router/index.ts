import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ColumList from "../components/ColumList.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/board",
      name: "board",
      component: ColumList,
    },
  ],
});

export default router;
